import 'package:appbanhang/models/product_modal.dart';
import 'package:appbanhang/provider/product.dart';

import 'package:appbanhang/widgets/feeds_product.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CategoryFeedsScreen extends StatelessWidget {
  static const routeName = '/CategoryFeedsScreen';
  const CategoryFeedsScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final productsProvider = Provider.of<Products>(context, listen: false);
    final categoryName = ModalRoute.of(context).settings.arguments as String;
    print(categoryName);
    final productsList= productsProvider.findByCategory(categoryName);
    return Scaffold(
      body: GridView.count(
        crossAxisCount: 2,
        childAspectRatio: 240 / 290,
        crossAxisSpacing: 8,
        mainAxisSpacing: 8,
        children: List.generate(productsList.length, (index) {
          return ChangeNotifierProvider.value(
            value: productsList[index],
            child: FeedProducts(

            ),
          );
        }),
      ),

    );
  }
}
