import 'package:flutter/material.dart';
class CartEmpty extends StatelessWidget {
  const CartEmpty({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: double.infinity,
          height: MediaQuery.of(context).size.height *0.4,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/emptycart.png"),
              fit: BoxFit.fill,
            )
          ),
        ),
        Text("Your Cart Is Empty",
        style: TextStyle(
          color: Theme.of(context).textSelectionColor,
          fontSize: 36,
          fontWeight: FontWeight.bold,
        ),),
        SizedBox(
          height: 10,
        ),
        Text("Looks like You did\t \n add anything to your cart yet",
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.grey,
            fontSize: 26,
            fontWeight: FontWeight.bold,
          ),),
        SizedBox(
          height: 30,
        ),
        Container(
          height: MediaQuery.of(context).size.height *0.06,
          width: MediaQuery.of(context).size.height *0.4,
          child: RaisedButton(
              onPressed: (){},
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(16),
              side: BorderSide(color: Colors.red),
    ),
    color: Colors.redAccent,
              child: Text("Shop Now",
                style: TextStyle(
                  color: Theme.of(context).textSelectionColor,
                  fontSize: 26,
                  fontWeight: FontWeight.bold,
                ),),
            ),
        ),
      ],
    );
  }
}
