import 'package:flutter/material.dart';
class WishlistEmpty extends StatelessWidget {
  const WishlistEmpty({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: double.infinity,
          height: MediaQuery.of(context).size.height *0.4,
          decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/empty-wishlist.png"),
                fit: BoxFit.fill,
              )
          ),
        ),
        Text("Your Wishlist Is Empty",
          style: TextStyle(
            color: Theme.of(context).textSelectionColor,
            fontSize: 36,
            fontWeight: FontWeight.bold,
          ),),
        SizedBox(
          height: 10,
        ),
        Text("Explore more and shortlist some items",
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.grey,
            fontSize: 26,
            fontWeight: FontWeight.bold,
          ),),
        SizedBox(
          height: 30,
        ),
        Container(
          height: MediaQuery.of(context).size.height *0.06,
          width: MediaQuery.of(context).size.height *0.4,
          child: RaisedButton(
            onPressed: (){},
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(16),
              side: BorderSide(color: Colors.red),
            ),
            color: Colors.redAccent,
            child: Text("Add a wish",
              style: TextStyle(
                color: Theme.of(context).textSelectionColor,
                fontSize: 26,
                fontWeight: FontWeight.bold,
              ),),
          ),
        ),
      ],
    );
  }
}
