import 'package:appbanhang/models/product_modal.dart';
import 'package:appbanhang/provider/product.dart';
import 'package:appbanhang/provider/product.dart';
import 'package:appbanhang/widgets/feeds_product.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Feeds extends StatelessWidget {
  static const routeName = '/Feeds';
  const Feeds({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
 final productsProvider = Provider.of<Products>(context);
 List<Product> productList = productsProvider.products;
    return Scaffold(
      body: GridView.count(
      crossAxisCount: 2,
      childAspectRatio: 240 / 290,
      crossAxisSpacing: 8,
      mainAxisSpacing: 8,
      children: List.generate(productList.length, (index) {
        return ChangeNotifierProvider.value(
          value: productList[index],
          child: FeedProducts(
            
          ),
        );
      }),
    ),

    );
  }
}
