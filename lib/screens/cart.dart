import 'package:appbanhang/icons/my_icons.dart';
import 'package:flutter/material.dart';

import '../widgets/cart_empty.dart';
import '../widgets/cart_full.dart';
class CartScreen extends StatelessWidget {
  static const routeName = '/CartScreen';
  const CartScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List products =[];
    return products.isEmpty ? Scaffold(
      body:CartEmpty(),
    ): Scaffold(
      bottomSheet: checkoutSection(context ),
      appBar: AppBar(
        title: Text("Cart Items Count"),
        actions: [
          IconButton(icon: Icon(MyAppIcons.trash), onPressed: (){})
        ],
      ),
      body: ListView.builder(
          itemCount: 5,
          itemBuilder: (BuildContext context,int index ){
            return CartFull();
          }),
    );
  }
  Widget checkoutSection(BuildContext context ){
    return Container(
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(
            color: Colors.grey,width: 0.5
          )
        )
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
        Expanded(
          flex: 2,
          child: Material(
            borderRadius: BorderRadius.circular(30),
            color: Colors.red,
            child: InkWell(
              borderRadius: BorderRadius.circular(30),
              onTap: (){},
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text("Checkout",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 18,fontWeight: FontWeight.bold),),
              ),
            ),
          ),
        ),
          Spacer(),
          Text("Total",style: TextStyle(color: Theme.of(context).textSelectionColor,fontSize: 18,fontWeight: FontWeight.bold),),
          Text("Us \$123456",style: TextStyle(color: Colors.blue,fontSize: 18,fontWeight: FontWeight.bold),),
        ],
      ),
    );
  }
}
