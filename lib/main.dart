import 'package:appbanhang/icons/dark_theme.dart';
import 'package:appbanhang/provider/dark_theme_provider.dart';
import 'package:appbanhang/provider/product.dart';
import 'package:appbanhang/screens/bottom_bar.dart';
import 'package:appbanhang/screens/cart.dart';
import 'package:appbanhang/screens/feeds.dart';
import 'package:appbanhang/screens/wishlist.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'inner_screen/category_feeds.dart';
import 'inner_screen/product_detail.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  void getCurrentAppTheme() async{
    themeChangeProvider.darkTheme = await themeChangeProvider.darkThemePreferences.getTheme();
  }
  void initState(){
    getCurrentAppTheme();
    super.initState();
  }
  DarkThemeProvider themeChangeProvider = DarkThemeProvider();

  @override
  Widget build(BuildContext context) {
   return MultiProvider(
     providers: [
       ChangeNotifierProvider(create: (_) => themeChangeProvider,),
       ChangeNotifierProvider(create: (_) => Products(),), // đặt tên như
     ],
    child: Consumer<DarkThemeProvider>(
      builder: (context,themeData, snapshot) {

        return MaterialApp(
          title: 'Flutter Demo',
          theme: Styles.themeData(themeChangeProvider.darkTheme, context),
          home: BottomBarScreen(),
          routes: {
            //   '/': (ctx) => LandingPage(),
            // BrandNavigationRailScreen.routeName: (ctx) =>
            //     BrandNavigationRailScreen(),
            CartScreen.routeName: (ctx) => CartScreen(),
            Feeds.routeName: (ctx) => Feeds(),
            WishlistScreen.routeName: (ctx) => WishlistScreen(),
            ProductDetails.routeName: (ctx) => ProductDetails(),
            CategoryFeedsScreen.routeName: (ctx) => CategoryFeedsScreen(),
          },
        );
      }
    ),
   );
  }
}
